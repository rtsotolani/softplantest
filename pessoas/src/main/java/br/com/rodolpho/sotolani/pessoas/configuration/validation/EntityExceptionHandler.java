package br.com.rodolpho.sotolani.pessoas.configuration.validation;

import br.com.rodolpho.sotolani.pessoas.exception.CidadeNotFoundException;
import br.com.rodolpho.sotolani.pessoas.exception.PaisNotFoundException;
import br.com.rodolpho.sotolani.pessoas.exception.PessoaNotFoundException;
import br.com.rodolpho.sotolani.pessoas.exception.UsuarioNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static br.com.rodolpho.sotolani.pessoas.configuration.validation.ClientErrorResponse.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class EntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(PessoaNotFoundException.class)
    public ClientErrorResponse handlePessoaNotFound(PessoaNotFoundException e) {
        return ClientErrorResponse.singleMessage(PERSON_DOES_NOT_EXIST);
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(UsuarioNotFoundException.class)
    public ClientErrorResponse handleUsuarioNotFound(UsuarioNotFoundException e) {
        return ClientErrorResponse.singleMessage(USER_DOES_NOT_EXIST);
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(PaisNotFoundException.class)
    public ClientErrorResponse handlePaisNotFound(PaisNotFoundException e) {
        return ClientErrorResponse.singleMessage(NATION_DOES_NOT_EXIST);
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(CidadeNotFoundException.class)
    public ClientErrorResponse handleCidadeNotFound(CidadeNotFoundException e) {
        return ClientErrorResponse.singleMessage(CITY_DOES_NOT_EXIST);
    }

}