package br.com.rodolpho.sotolani.pessoas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class ApplicationController {

    @GetMapping("/source")
    @ResponseBody
    public String getURLBitbucket() {
        return "https://bitbucket.org/rtsotolani/softplantest/src/master/";
    }

    @GetMapping("/")
    public RedirectView index() {
        return new RedirectView("swagger-ui.html");
    }

}
