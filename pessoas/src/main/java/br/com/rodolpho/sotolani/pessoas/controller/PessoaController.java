package br.com.rodolpho.sotolani.pessoas.controller;

import br.com.rodolpho.sotolani.pessoas.controller.dto.PessoaDto;
import br.com.rodolpho.sotolani.pessoas.controller.filter.PessoaFilter;
import br.com.rodolpho.sotolani.pessoas.controller.form.PessoaForm;
import br.com.rodolpho.sotolani.pessoas.domain.Pessoa;
import br.com.rodolpho.sotolani.pessoas.service.PessoaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    private PessoaService service;

    @Autowired
    public PessoaController(PessoaService pessoaService) {
        service = pessoaService;
    }

    @GetMapping
    @Cacheable(value = "listaDePessoas")
    public Page<PessoaDto> list(PessoaFilter pessoaFilter,
                                @PageableDefault(sort = "nome", direction = Sort.Direction.DESC)
                                        Pageable paginacao) {
        Page<Pessoa> pessoas = service.findByNome(pessoaFilter, paginacao);
        return PessoaDto.converter(pessoas);
    }

    @GetMapping("/{idPessoa}")
    public ResponseEntity<PessoaDto> view(@PathVariable Long idPessoa) {
        return ResponseEntity.ok(
                new PessoaDto(
                        service.findById(idPessoa)));
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "listaDePessoas", allEntries = true)
    public ResponseEntity<PessoaDto> create(@RequestBody @Valid PessoaForm pessoaForm,
                                            UriComponentsBuilder uriBuilder) {
        try {
            Pessoa pessoa = service.save(pessoaForm);
            URI uri = uriBuilder.path("/pessoas/{id}").buildAndExpand(pessoa.getId()).toUri();
            return ResponseEntity.created(uri).body(new PessoaDto(pessoa));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{idPessoa}")
    @Transactional
    @CacheEvict(value = "listaDePessoas", allEntries = true)
    public ResponseEntity<PessoaDto> update(@PathVariable Long idPessoa,
                                            @RequestBody @Valid PessoaForm pessoaForm) {

        Pessoa pessoa = null;
        try {
            pessoa = this.service.update(idPessoa, pessoaForm);
            return ResponseEntity.ok(new PessoaDto(pessoa));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{idPessoa}")
    @Transactional
    @CacheEvict(value = "listaDePessoas", allEntries = true)
    public ResponseEntity<?> delete(@PathVariable Long idPessoa) {

        try {
            this.service.delete(idPessoa);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

}
