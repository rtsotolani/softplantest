package br.com.rodolpho.sotolani.pessoas.controller;

import br.com.rodolpho.sotolani.pessoas.controller.dto.UsuarioDto;
import br.com.rodolpho.sotolani.pessoas.controller.form.UsuarioForm;
import br.com.rodolpho.sotolani.pessoas.domain.Usuario;
import br.com.rodolpho.sotolani.pessoas.service.UsuarioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;

@Slf4j
@Controller
@RequestMapping("/users")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping
    @Cacheable(value = "listaDeUsuarios")
    public Page<UsuarioDto> list(@RequestParam(required = false) String nome,
                                 @PageableDefault(sort = "nome", direction = Sort.Direction.DESC)
                                         Pageable paginacao) {
        Page<Usuario> usuarios = service.findByNome(nome, paginacao);
        return UsuarioDto.converter(usuarios);
    }

    @GetMapping("/{idUsuario}")
    public ResponseEntity<UsuarioDto> viewById(@PathVariable Long idUsuario) {
        return ResponseEntity.ok(
                new UsuarioDto(
                        service.findById(idUsuario)));
    }

    @GetMapping("/{username}")
    public ResponseEntity<UsuarioDto> viewByUsername(@PathVariable String username) {
        return ResponseEntity.ok(
                new UsuarioDto(
                        service.findByUsername(username)));
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "listaDeUsuarios", allEntries = true)
    public ResponseEntity<UsuarioDto> create(@RequestBody @Valid UsuarioForm usuarioForm,
                                             UriComponentsBuilder uriBuilder) {
        try {
            Usuario usuario = service.save(usuarioForm);
            URI uri = uriBuilder.path("/usuarios/{id}").buildAndExpand(usuario.getId()).toUri();
            return ResponseEntity.created(uri).body(new UsuarioDto(usuario));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{idUsuario}")
    @Transactional
    @CacheEvict(value = "listaDeUsuarios", allEntries = true)
    public ResponseEntity<UsuarioDto> update(@PathVariable Long idUsuario,
                                             @RequestBody @Valid UsuarioForm usuarioForm) {

        try {
            Usuario usuario = this.service.update(idUsuario, usuarioForm);
            return ResponseEntity.ok(new UsuarioDto(usuario));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{idUsuario}")
    @Transactional
    @CacheEvict(value = "listaDeUsuarios", allEntries = true)
    public ResponseEntity<?> delete(@PathVariable Long idUsuario) {

        try {
            this.service.delete(idUsuario);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

}
