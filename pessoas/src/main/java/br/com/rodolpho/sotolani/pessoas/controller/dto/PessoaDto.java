package br.com.rodolpho.sotolani.pessoas.controller.dto;

import br.com.rodolpho.sotolani.pessoas.domain.Pessoa;
import br.com.rodolpho.sotolani.pessoas.enums.SexoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PessoaDto {

    private Long id;
    private String nome;
    private SexoEnum sexo;
    private String email;
    private Date dataNascimento;
    private Long idNaturalidade;
    private Long idNacionalidade;
    private String cpf;
    private Date dataCadastro;
    private Date dataAtualizacao;

    public PessoaDto(Pessoa pessoa) {
        this.id = pessoa.getId();
        this.nome = pessoa.getNome();
        this.sexo = pessoa.getSexo();
        this.email = pessoa.getEmail();
        this.dataNascimento = pessoa.getDataNascimento();
        this.idNaturalidade = pessoa.getNaturalidade().getId();
        this.idNacionalidade = pessoa.getNacionalidade().getId();
        this.cpf = pessoa.getCpf();
        this.dataCadastro = pessoa.getDataCadastro();
        this.dataAtualizacao = pessoa.getDataAtualizacao();
    }

    public static Page<PessoaDto> converter(Page<Pessoa> pessoaList) {
        return pessoaList.map(PessoaDto::new);
    }
}
