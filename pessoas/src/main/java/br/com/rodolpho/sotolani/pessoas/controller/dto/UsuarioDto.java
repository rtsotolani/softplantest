package br.com.rodolpho.sotolani.pessoas.controller.dto;

import br.com.rodolpho.sotolani.pessoas.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioDto {

    private Long id;
    private String nome;
    private String email;
    private String username;

    public UsuarioDto(Usuario usuario) {
        this.id = usuario.getId();
        this.nome = usuario.getNome();
        this.email = usuario.getEmail();
        this.username = usuario.getUsername();
    }

    public static Page<UsuarioDto> converter(Page<Usuario> usuarioList) {
        return usuarioList.map(UsuarioDto::new);
    }
}
