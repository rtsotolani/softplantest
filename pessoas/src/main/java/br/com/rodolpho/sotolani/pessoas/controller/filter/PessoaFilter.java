package br.com.rodolpho.sotolani.pessoas.controller.filter;

import br.com.rodolpho.sotolani.pessoas.enums.SexoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PessoaFilter {

    private String nome = "";
    private SexoEnum sexo;
    private String cpf = "";
}
