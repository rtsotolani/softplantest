package br.com.rodolpho.sotolani.pessoas.controller.form;

import lombok.Data;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import javax.validation.constraints.NotEmpty;

import static br.com.rodolpho.sotolani.pessoas.configuration.validation.ClientErrorResponse.PASSWORD_IS_EMPTY;
import static br.com.rodolpho.sotolani.pessoas.configuration.validation.ClientErrorResponse.USERNAME_IS_EMPTY;

@Data
public class LoginForm {

    @NotEmpty(message = USERNAME_IS_EMPTY)
    private String email;

    @NotEmpty(message = PASSWORD_IS_EMPTY)
    private String senha;

    public UsernamePasswordAuthenticationToken converter() {
        return new UsernamePasswordAuthenticationToken(this.getEmail(), this.getSenha());
    }

}
