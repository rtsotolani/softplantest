package br.com.rodolpho.sotolani.pessoas.controller.form;

import br.com.rodolpho.sotolani.pessoas.enums.SexoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PessoaForm {

    @NotBlank(message = "O campo Nome é obrigatório!")
    @Length(max = 150, message = "O campo Nome não pode ter mais que 150 caracteres!")
    private String nome;

    private SexoEnum sexo;

    @Email(message = "O E-mail informado está incorreto!")
    @Length(max = 150, message = "O campo E-mail não pode ter mais que 150 caracteres!")
    private String email;

    @NotNull(message = "O campo Data de Nascimento é obrigatório!")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dataNascimento;

    private Long idNaturalidade;

    private Long idNacionalidade;

    @NotNull(message = "O campo CPF é obrigatório!")
    @Length(max = 14, message = "O campo CPF não pode ter mais que 14 caracteres!")
    @CPF(message = "O CPF é inválido")
    private String cpf;
}
