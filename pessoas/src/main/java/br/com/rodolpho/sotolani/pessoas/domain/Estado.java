package br.com.rodolpho.sotolani.pessoas.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Estado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String nome;

    private String sigla;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "cod_pais")
    private Pais pais;
}
