package br.com.rodolpho.sotolani.pessoas.domain;

import br.com.rodolpho.sotolani.pessoas.enums.SexoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Pessoa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "O campo Nome é obrigatório!")
    @Length(max = 150, message = "O campo Nome não pode ter mais que 150 caracteres!")
    private String nome;

    @Enumerated(EnumType.STRING)
    private SexoEnum sexo;

    @Email(message = "O E-mail informado está incorreto!")
    @Length(max = 150, message = "O campo E-mail não pode ter mais que 150 caracteres!")
    private String email;

    @NotNull(message = "O campo Data de Nascimento é obrigatório!")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dataNascimento;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "naturalidade")
    private Cidade naturalidade;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "nacionalidade")
    private Pais nacionalidade;

    @NotNull(message = "O campo CPF é obrigatório!")
    @Length(max = 14, message = "O campo CPF não pode ter mais que 14 caracteres!")
    @CPF(message = "O CPF é inválido")
    @Column(unique = true)
    private String cpf;

    @Builder.Default
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date dataCadastro = new Date();

    @Builder.Default
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAtualizacao = new Date();

}
