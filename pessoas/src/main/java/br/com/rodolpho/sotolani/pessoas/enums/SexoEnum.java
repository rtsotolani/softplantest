package br.com.rodolpho.sotolani.pessoas.enums;

import lombok.Getter;

@Getter
public enum SexoEnum {

    MASCULINO("M", "Masculino"),
    FEMININO("F", "Feminino");

    private String codigo;
    private String descricao;

    SexoEnum(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }
}