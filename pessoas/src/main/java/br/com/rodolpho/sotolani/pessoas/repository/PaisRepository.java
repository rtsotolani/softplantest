package br.com.rodolpho.sotolani.pessoas.repository;

import br.com.rodolpho.sotolani.pessoas.domain.Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Long> {

}
