package br.com.rodolpho.sotolani.pessoas.repository;

import br.com.rodolpho.sotolani.pessoas.domain.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long> {

}
