package br.com.rodolpho.sotolani.pessoas.repository;

import br.com.rodolpho.sotolani.pessoas.domain.Pessoa;
import br.com.rodolpho.sotolani.pessoas.enums.SexoEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    @Query("SELECT p " +
            "FROM Pessoa as p " +
            "WHERE (UPPER(p.nome) LIKE ?1) " +
            "AND (UPPER(p.cpf) LIKE ?2 ) " +
            "AND (p.sexo = ?3 OR ?3 IS NULL)")
    Page<Pessoa> findByNomeCpfSexo(String nome, String cpf, SexoEnum sexo, Pageable pageable);
}
