package br.com.rodolpho.sotolani.pessoas.service;

import br.com.rodolpho.sotolani.pessoas.domain.Cidade;
import br.com.rodolpho.sotolani.pessoas.exception.CidadeNotFoundException;
import br.com.rodolpho.sotolani.pessoas.repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class CidadeService implements Serializable {

    @Autowired
    private CidadeRepository repository;

    public Cidade findById(Long idCidade) {
        return this.repository.findById(idCidade).orElseThrow(CidadeNotFoundException::new);
    }
}
