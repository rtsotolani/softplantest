package br.com.rodolpho.sotolani.pessoas.service;

import br.com.rodolpho.sotolani.pessoas.domain.Pais;
import br.com.rodolpho.sotolani.pessoas.exception.PaisNotFoundException;
import br.com.rodolpho.sotolani.pessoas.repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class PaisService implements Serializable {

    @Autowired
    private PaisRepository repository;

    public Pais findById(Long idPais) {
        return this.repository.findById(idPais).orElseThrow(PaisNotFoundException::new);
    }
}
