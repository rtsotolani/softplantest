package br.com.rodolpho.sotolani.pessoas.service;

import br.com.rodolpho.sotolani.pessoas.controller.filter.PessoaFilter;
import br.com.rodolpho.sotolani.pessoas.controller.form.PessoaForm;
import br.com.rodolpho.sotolani.pessoas.domain.Cidade;
import br.com.rodolpho.sotolani.pessoas.domain.Pais;
import br.com.rodolpho.sotolani.pessoas.domain.Pessoa;
import br.com.rodolpho.sotolani.pessoas.exception.PessoaNotFoundException;
import br.com.rodolpho.sotolani.pessoas.repository.PessoaRepository;
import br.com.rodolpho.sotolani.pessoas.utils.SQLUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;

@Service
public class PessoaService implements Serializable {

    @Autowired
    private PessoaRepository repository;

    @Autowired
    private PaisService paisService;

    @Autowired
    private CidadeService cidadeService;

    public Pessoa findById(Long idPessoa) {
        return this.repository.findById(idPessoa).orElseThrow(PessoaNotFoundException::new);
    }

    public Page<Pessoa> findAll(Pageable pageable) {
        return this.repository.findAll(pageable);
    }

    public Pessoa save(PessoaForm pessoaForm) {
        Pais pais = paisService.findById(pessoaForm.getIdNacionalidade());
        Cidade cidade = cidadeService.findById(pessoaForm.getIdNaturalidade());

        Pessoa pessoaBuilder = Pessoa.builder()
                .cpf(pessoaForm.getCpf())
                .dataAtualizacao(new Date())
                .dataCadastro(new Date())
                .dataNascimento(pessoaForm.getDataNascimento())
                .email(pessoaForm.getEmail())
                .nacionalidade(pais)
                .naturalidade(cidade)
                .nome(pessoaForm.getNome())
                .sexo(pessoaForm.getSexo())
                .build();

        return this.repository.save(pessoaBuilder);
    }

    public void delete(Long idPessoa) {
        this.repository.delete(findById(idPessoa));
    }

    public Page<Pessoa> findByNome(PessoaFilter pessoaFilter, Pageable pageable) {
        return this.repository.findByNomeCpfSexo(
                SQLUtils.formatterString(pessoaFilter.getNome()),
                SQLUtils.formatterString(pessoaFilter.getCpf()),
                pessoaFilter.getSexo(),
                pageable);
    }

    public Pessoa update(Long idPessoa, PessoaForm pessoaForm) {
        Pessoa pessoaOld = findById(idPessoa);

        Pais pais = paisService.findById(pessoaForm.getIdNacionalidade());
        Cidade cidade = cidadeService.findById(pessoaForm.getIdNaturalidade());

        Pessoa pessoaBuilder = Pessoa.builder()
                .id(pessoaOld.getId())
                .cpf(pessoaForm.getCpf())
                .dataAtualizacao(new Date())
                .dataCadastro(pessoaOld.getDataCadastro())
                .dataNascimento(pessoaForm.getDataNascimento())
                .email(pessoaForm.getEmail())
                .nacionalidade(pais)
                .naturalidade(cidade)
                .nome(pessoaForm.getNome())
                .sexo(pessoaForm.getSexo())
                .build();

        return this.repository.save(pessoaBuilder);
    }
}
