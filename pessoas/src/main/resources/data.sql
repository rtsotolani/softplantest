INSERT INTO public.usuario(email, nome, password, username)
VALUES ('teste@softplan.com.br', 'Administrador', '$2y$12$44KPHViErMC0s5oWLxtOCeV6jzbkBVLaX5rcGfsoS7V41cRnSdgTe', 'admin')
ON CONFLICT (username) DO NOTHING;

INSERT INTO public.pais(nome, sigla) VALUES ('Brasil', 'BR') ON CONFLICT (nome) DO NOTHING;

INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Acre', 'AC', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Alagoas', 'AL', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Amapá', 'AP', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Amazonas', 'AM', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Bahia', 'BA', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Ceará', 'CE', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Distrito Federal', 'DF', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Espírito Santo', 'ES', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Goiás', 'GO', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Maranhão', 'MA', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Mato Grosso', 'MT', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Mato Grosso do Sul', 'MS', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Minas Gerais', 'MG', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Pará', 'PA', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Paraíba', 'PB', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Paraná', 'PR', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Pernambuco', 'PE', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Piauí', 'PI', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Rio de Janeiro', 'RJ', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Rio Grande do Norte', 'RN', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Rio Grande do Sul', 'RS', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Rondônia', 'RO', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Roraima', 'RR', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Santa Catarina', 'SC', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('São Paulo', 'SP', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Sergipe', 'SE', 1) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.estado(nome, sigla, cod_pais) VALUES ('Tocantins', 'TO', 1) ON CONFLICT (nome) DO NOTHING;


INSERT INTO public.cidade(nome, cod_estado) VALUES ('Campo Grande', 12) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Dourados', 12) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Nioaque', 12) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Bataguassu', 12) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('São Paulo', 25) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('São Bernardo', 25) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Santo André', 25) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('São Jose dos Campos', 25) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Florianópolis', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Joinvile', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Chapecó', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Blumenau', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Balneário Camboriú', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Brusque', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Itapema', 24) ON CONFLICT (nome) DO NOTHING;
INSERT INTO public.cidade(nome, cod_estado) VALUES ('Criciúma', 24) ON CONFLICT (nome) DO NOTHING;
