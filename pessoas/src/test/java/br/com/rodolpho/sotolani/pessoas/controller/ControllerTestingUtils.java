//package br.com.rodolpho.sotolani.pessoas.controller;
//
//import org.junit.Assert;
//import org.springframework.http.HttpStatus;
//import org.springframework.test.web.servlet.MvcResult;
//
//import java.io.IOException;
//
//import static org.hamcrest.Matchers.containsString;
//import static org.junit.Assert.assertEquals;
//
//class ControllerTestingUtils {
//
//    static void assertStatusIs(HttpStatus expectedStatus, MvcResult mvcResult) {
//        assertEquals(expectedStatus.value(), mvcResult.getResponse().getStatus());
//    }
//
//    static void assertResponseContains(String expectedContent, MvcResult mvcResult) throws IOException {
//        String json = mvcResult.getResponse().getContentAsString();
//        Assert.assertThat(json, containsString(expectedContent));
//    }
//
//}
