//package br.com.rodolpho.sotolani.pessoas.controller;
//
//import br.com.rodolpho.sotolani.pessoas.configuration.validation.GlobalExceptionHandler;
//import br.com.rodolpho.sotolani.pessoas.mock.PessoaMockFactory;
//import br.com.rodolpho.sotolani.pessoas.service.PessoaService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.dao.DataIntegrityViolationException;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import static br.com.rodolpho.sotolani.pessoas.configuration.validation.ClientErrorResponse.INCONSISTENT_DATA_CHECK_RELATIONSHIPS;
//import static br.com.rodolpho.sotolani.pessoas.controller.ControllerTestingUtils.assertResponseContains;
//import static br.com.rodolpho.sotolani.pessoas.controller.ControllerTestingUtils.assertStatusIs;
//import static br.com.rodolpho.sotolani.pessoas.controller.dto.ServerErrorDto.SOMETHING_WENT_WRONG;
//import static org.mockito.Mockito.when;
//import static org.springframework.http.HttpStatus.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class ExceptionHandlerTests {
//    private static final String ENDPOINT_BOOK_ID = "/books/{id}";
//    private static final String ENDPOINT_BOOKS = "/books";
//
//    @Mock
//    private PessoaService pessoaServiceMock;
//    private MockMvc mockMvc;
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//        PessoaController controller = new PessoaController(pessoaServiceMock);
//        mockMvc = MockMvcBuilders
//            .standaloneSetup(controller)
//            .setControllerAdvice(new GlobalExceptionHandler())
//            .build();
//    }
//
//    @Test
//    public void getInexistentBook_StatusShouldBe404() throws Exception {
//        long id = 1L;
//        when(pessoaServiceMock.getBookDetails(id))
//            .thenThrow(BookNotFoundException.class);
//
//        MvcResult mvcResult = mockMvc.perform(
//            get(ENDPOINT_BOOK_ID, id)
//        ).andReturn();
//
//        assertStatusIs(NOT_FOUND, mvcResult);
//    }
//
//    @Test
//    public void serviceThrowsNullPointer_StatusShouldBe500WithMessage() throws Exception {
//        long id = 1L;
//        when(pessoaServiceMock.getBookDetails(id))
//            .thenThrow(NullPointerException.class);
//
//        MvcResult mvcResult = mockMvc.perform(
//            get(ENDPOINT_BOOK_ID, id)
//        ).andReturn();
//
//        assertStatusIs(INTERNAL_SERVER_ERROR, mvcResult);
//        assertResponseContains(SOMETHING_WENT_WRONG, mvcResult);
//    }
//
//    @Test
//    public void serviceThrowsDataIntegrityViolation_StatusShouldBe400WithMessage() throws Exception {
//        BookRequest bookRequest = BookMockFactory.createBookOK1();
//        when(pessoaServiceMock.insertBook(bookRequest))
//            .thenThrow(DataIntegrityViolationException.class);
//
//        MvcResult mvcResult = mockMvc.perform(
//            post(ENDPOINT_BOOKS)
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(PessoaControllerTests.getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(BAD_REQUEST, mvcResult);
//        assertResponseContains(INCONSISTENT_DATA_CHECK_RELATIONSHIPS, mvcResult);
//    }
//
//    @Test
//    public void bookWithoutAuthorId_StatusShouldBe400WithMessage() throws Exception {
//        BookRequest bookRequest = PessoaMockFactory.createBookWithouAuthorId();
//
//        MvcResult mvcResult = mockMvc.perform(
//            post(ENDPOINT_BOOKS)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(PessoaControllerTests.getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(BAD_REQUEST, mvcResult);
//        assertResponseContains(BOOK_AUTHOR_ID_IS_NULL, mvcResult);
//    }
//
//    @Test
//    public void bookWithoutIsbn_StatusShouldBe400WithMessage() throws Exception {
//        BookRequest bookRequest = BookMockFactory.createBookWithoutIsbn();
//
//        MvcResult mvcResult = mockMvc.perform(
//            post(ENDPOINT_BOOKS)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(PessoaControllerTests.getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(BAD_REQUEST, mvcResult);
//        assertResponseContains(BOOK_ISBN_IS_EMPTY, mvcResult);
//    }
//}
