//package br.com.rodolpho.sotolani.pessoas.controller;
//
//import br.com.rodolpho.sotolani.pessoas.controller.dto.PessoaDto;
//import br.com.rodolpho.sotolani.pessoas.controller.form.PessoaForm;
//import br.com.rodolpho.sotolani.pessoas.domain.Pessoa;
//import br.com.rodolpho.sotolani.pessoas.mock.PessoaMockFactory;
//import br.com.rodolpho.sotolani.pessoas.service.PessoaService;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.humanoo.booksapi.book.PessoaService;
//import com.humanoo.booksapi.controller.book.PessoaController;
//import com.humanoo.booksapi.controller.book.request.PessoaRequest;
//import com.humanoo.booksapi.controller.book.response.PessoaResponse;
//import com.humanoo.booksapi.controller.book.response.MinimalPessoaResponse;
//import com.humanoo.booksapi.mock.PessoaMockFactory;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//
//import static br.com.rodolpho.sotolani.pessoas.controller.ControllerTestingUtils.*;
//import static com.humanoo.booksapi.controller.ControllerTestingUtils.*;
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.when;
//import static org.springframework.http.HttpStatus.CREATED;
//import static org.springframework.http.HttpStatus.OK;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class PessoaControllerTests {
//
//    private static final String ENDPOINT_PERSON_ID = "/pessoas/{id}";
//    private static final String ENDPOINT_PERSON = "/pessoas";
//
//    @Mock
//    private PessoaService pessoaServiceMock;
//    private MockMvc mockMvc;
//    private static ObjectMapper objectMapper = new ObjectMapper();
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//        PessoaController controller = new PessoaController(pessoaServiceMock);
//        mockMvc = MockMvcBuilders
//            .standaloneSetup(controller)
//            .build();
//    }
//
//    @Test
//    public void getPessoaOK_statusShouldBe200AndPessoaReturned() throws Exception {
//        PessoaDto pessoaDto = PessoaMockFactory.createPessoaResponse1();
//        long id = pessoaDto.getId();
//        when(pessoaServiceMock.getPessoaDetails(id))
//            .thenReturn(pessoaDto);
//
//        MvcResult mvcResult = mockMvc.perform(
//            get(ENDPOINT_PERSON_ID, id)
//        ).andDo(print()).andReturn();
//
//        assertStatusIs(OK, mvcResult);
//        assertPessoaWasReturned(pessoaDto, mvcResult);
//    }
//
//    @Test
//    public void insertPessoaOK_statusShouldBe201AndPessoaReturned() throws Exception {
//        PessoaRequest bookRequest = PessoaMockFactory.createPessoaOK1();
//        PessoaResponse bookResponse = PessoaMockFactory.createPessoaResponse1();
//        when(pessoaServiceMock.insertPessoa(bookRequest))
//            .thenReturn(bookResponse);
//
//        MvcResult mvcResult = mockMvc.perform(
//            post(ENDPOINT_PERSON)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(CREATED, mvcResult);
//        assertPessoaWasReturned(bookResponse, mvcResult);
//    }
//
//    @Test
//    public void updatePessoaOK_statusShouldBe200AndPessoaReturned() throws Exception {
//        PessoaDto bookRequest = PessoaMockFactory.createPessoaOK1();
//        Pessoa bookResponse = PessoaMockFactory.createPessoaResponse1();
//        Long id = bookResponse.getId();
//        when(pessoaServiceMock.updatePessoa(bookRequest, id))
//            .thenReturn(bookResponse);
//
//        MvcResult mvcResult = mockMvc.perform(
//            put(ENDPOINT_PERSON_ID, id)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(OK, mvcResult);
//        assertPessoaWasReturned(bookResponse, mvcResult);
//    }
//
//    @Test
//    public void listPessoasOK_statusShouldBe200AndPessoasReturned() throws Exception {
//        List<MinimalPessoaResponse> books = PessoaMockFactory.createListOfMinimalPessoaResponses();
//        when(pessoaServiceMock.getAllPessoas())
//            .thenReturn(books);
//
//        MvcResult mvcResult = mockMvc.perform(
//            get(ENDPOINT_PERSON)
//        ).andReturn();
//
//        assertStatusIs(OK, mvcResult);
//        assertPessoasWereReturned(books, mvcResult);
//    }
//
//    static String getJson(BookRequest bookRequest) throws JsonProcessingException {
//        return objectMapper.writeValueAsString(bookRequest);
//    }
//
//    static void assertBookWasReturned(BookResponse expectedBookResponse, MvcResult mvcResult) throws IOException {
//        String json = mvcResult.getResponse().getContentAsString();
//        BookResponse bookResponse = objectMapper.readValue(json, BookResponse.class);
//        assertEquals(expectedBookResponse, bookResponse);
//    }
//
//    static void assertBooksWereReturned(List<MinimalBookResponse> expectedBooks, MvcResult mvcResult) throws IOException {
//        String json = mvcResult.getResponse().getContentAsString();
//        List<MinimalBookResponse> actualBooks = Arrays.asList(objectMapper.readValue(json, MinimalBookResponse[].class));
//        assertEquals(expectedBooks, actualBooks);
//    }
//
//}
