//package br.com.rodolpho.sotolani.pessoas.controller;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.humanoo.booksapi.book.BookService;
//import com.humanoo.booksapi.controller.book.BookController;
//import com.humanoo.booksapi.controller.book.request.BookRequest;
//import com.humanoo.booksapi.controller.book.response.BookResponse;
//import com.humanoo.booksapi.controller.book.response.MinimalBookResponse;
//import com.humanoo.booksapi.mock.BookMockFactory;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//
//import static com.humanoo.booksapi.controller.ControllerTestingUtils.*;
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.when;
//import static org.springframework.http.HttpStatus.CREATED;
//import static org.springframework.http.HttpStatus.OK;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class UsuarioControllerTests {
//    private static final String ENDPOINT_BOOK_ID = "/books/{id}";
//    private static final String ENDPOINT_BOOKS = "/books";
//
//    @Mock
//    private BookService bookServiceMock;
//    private MockMvc mockMvc;
//    private static ObjectMapper objectMapper = new ObjectMapper();
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//        BookController controller = new BookController(bookServiceMock);
//        mockMvc = MockMvcBuilders
//            .standaloneSetup(controller)
//            .build();
//    }
//
//    @Test
//    public void getBookOK_statusShouldBe200AndBookReturned() throws Exception {
//        BookResponse bookResponse = BookMockFactory.createBookResponse1();
//        long id = bookResponse.getId();
//        when(bookServiceMock.getBookDetails(id))
//            .thenReturn(bookResponse);
//
//        MvcResult mvcResult = mockMvc.perform(
//            get(ENDPOINT_BOOK_ID, id)
//        ).andDo(print()).andReturn();
//
//        assertStatusIs(OK, mvcResult);
//        assertBookWasReturned(bookResponse, mvcResult);
//    }
//
//    @Test
//    public void insertBookOK_statusShouldBe201AndBookReturned() throws Exception {
//        BookRequest bookRequest = BookMockFactory.createBookOK1();
//        BookResponse bookResponse = BookMockFactory.createBookResponse1();
//        when(bookServiceMock.insertBook(bookRequest))
//            .thenReturn(bookResponse);
//
//        MvcResult mvcResult = mockMvc.perform(
//            post(ENDPOINT_BOOKS)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(CREATED, mvcResult);
//        assertBookWasReturned(bookResponse, mvcResult);
//    }
//
//    @Test
//    public void updateBookOK_statusShouldBe200AndBookReturned() throws Exception {
//        BookRequest bookRequest = BookMockFactory.createBookOK1();
//        BookResponse bookResponse = BookMockFactory.createBookResponse1();
//        Long id = bookResponse.getId();
//        when(bookServiceMock.updateBook(bookRequest, id))
//            .thenReturn(bookResponse);
//
//        MvcResult mvcResult = mockMvc.perform(
//            put(ENDPOINT_BOOK_ID, id)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(getJson(bookRequest))
//        ).andReturn();
//
//        assertStatusIs(OK, mvcResult);
//        assertBookWasReturned(bookResponse, mvcResult);
//    }
//
//    @Test
//    public void listBooksOK_statusShouldBe200AndBooksReturned() throws Exception {
//        List<MinimalBookResponse> books = BookMockFactory.createListOfMinimalBookResponses();
//        when(bookServiceMock.getAllBooks())
//            .thenReturn(books);
//
//        MvcResult mvcResult = mockMvc.perform(
//            get(ENDPOINT_BOOKS)
//        ).andReturn();
//
//        assertStatusIs(OK, mvcResult);
//        assertBooksWereReturned(books, mvcResult);
//    }
//
//    static String getJson(BookRequest bookRequest) throws JsonProcessingException {
//        return objectMapper.writeValueAsString(bookRequest);
//    }
//
//    static void assertBookWasReturned(BookResponse expectedBookResponse, MvcResult mvcResult) throws IOException {
//        String json = mvcResult.getResponse().getContentAsString();
//        BookResponse bookResponse = objectMapper.readValue(json, BookResponse.class);
//        assertEquals(expectedBookResponse, bookResponse);
//    }
//
//    static void assertBooksWereReturned(List<MinimalBookResponse> expectedBooks, MvcResult mvcResult) throws IOException {
//        String json = mvcResult.getResponse().getContentAsString();
//        List<MinimalBookResponse> actualBooks = Arrays.asList(objectMapper.readValue(json, MinimalBookResponse[].class));
//        assertEquals(expectedBooks, actualBooks);
//    }
//
//
//}
