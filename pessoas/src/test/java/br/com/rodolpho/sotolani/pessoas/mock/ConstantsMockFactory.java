package br.com.rodolpho.sotolani.pessoas.mock;

public class ConstantsMockFactory {

    public static Long PAIS_VALIDO = 1l;
    public static Long CIDADE_VALIDA = 5l;

    public static Long PAIS_INVALIDO = 5l;
    public static Long CIDADE_INVALIDA = 752l;

}
