package br.com.rodolpho.sotolani.pessoas.mock;

import br.com.rodolpho.sotolani.pessoas.controller.dto.PessoaDto;
import br.com.rodolpho.sotolani.pessoas.controller.form.PessoaForm;
import br.com.rodolpho.sotolani.pessoas.enums.SexoEnum;
import br.com.rodolpho.sotolani.pessoas.utils.DateUtils;

import java.util.Date;

import static br.com.rodolpho.sotolani.pessoas.mock.ConstantsMockFactory.*;

public class PessoaMockFactory {


    public static PessoaForm createOK1() {
        return PessoaForm.builder()
                .cpf("283.617.915-58")
                .dataNascimento(DateUtils.createDate(25, 9, 1986))
                .email("teste1@teste.gmail.com")
                .idNacionalidade(PAIS_VALIDO)
                .idNaturalidade(CIDADE_VALIDA)
                .nome("teste pessoa 1")
                .sexo(SexoEnum.MASCULINO)
                .build();
    }

    public static PessoaForm createOK2() {
        return PessoaForm.builder()
                .cpf("470.822.980-13")
                .dataNascimento(DateUtils.createDate(8, 10, 1995))
                .email("teste2@teste.gmail.com")
                .idNacionalidade(PAIS_VALIDO)
                .idNaturalidade(CIDADE_VALIDA)
                .nome("Teste Pessoa 2")
                .sexo(SexoEnum.FEMININO)
                .build();
    }

    public static PessoaForm createWithoutNome() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setNome("");
        return pessoaForm;
    }

    public static PessoaForm createWithDataNascimento() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setDataNascimento(null);
        return pessoaForm;
    }

    public static PessoaForm createWithCPF() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setCpf(null);
        return pessoaForm;
    }

    public static PessoaForm createInvalidEmail() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setEmail("teste6.teste.gmail.com");
        return pessoaForm;
    }

    public static PessoaForm createInvalidCPF() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setCpf("5421318132121311231");
        return pessoaForm;
    }

    public static PessoaForm createInvalidNacionalidade() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setIdNacionalidade(PAIS_INVALIDO);
        return pessoaForm;
    }

    public static PessoaForm createInvalidNaturalidade() {
        PessoaForm pessoaForm = createOK1();
        pessoaForm.setIdNaturalidade(CIDADE_INVALIDA);
        return pessoaForm;
    }

    public static PessoaDto createPessoaDto1() {
        return PessoaDto.builder()
                .cpf("283.617.915-58")
                .dataNascimento(DateUtils.createDate(25, 9, 1986))
                .email("teste1@teste.gmail.com")
                .idNacionalidade(PAIS_VALIDO)
                .idNaturalidade(CIDADE_VALIDA)
                .nome("teste pessoa 1")
                .sexo(SexoEnum.MASCULINO)

                .id(1L)
                .dataAtualizacao(new Date())
                .dataCadastro(new Date())
                .build();
    }

    public static PessoaDto createPessoaDto2() {
        return PessoaDto.builder()
                .cpf("470.822.980-13")
                .dataNascimento(DateUtils.createDate(8, 10, 1995))
                .email("teste2@teste.gmail.com")
                .idNacionalidade(PAIS_VALIDO)
                .idNaturalidade(CIDADE_VALIDA)
                .nome("Teste Pessoa 2")
                .sexo(SexoEnum.FEMININO)

                .id(2L)
                .dataAtualizacao(new Date())
                .dataCadastro(new Date())
                .build();
    }
}
