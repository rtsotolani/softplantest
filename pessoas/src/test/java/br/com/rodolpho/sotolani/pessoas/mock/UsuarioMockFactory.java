package br.com.rodolpho.sotolani.pessoas.mock;

import br.com.rodolpho.sotolani.pessoas.controller.dto.UsuarioDto;
import br.com.rodolpho.sotolani.pessoas.controller.form.UsuarioForm;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class UsuarioMockFactory {

    static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public static UsuarioForm createOK1() {
        return UsuarioForm
                .builder()
                .email("teste@gmail.com")
                .nome("Teste Usuario 1")
                .username("usuarioOk1")
                .password(passwordEncoder.encode("usuarioOk1"))
                .build();
    }

    public static UsuarioForm createOK2() {
        return UsuarioForm
                .builder()
                .email("teste@gmail.com")
                .nome("Teste Usuario 2")
                .username("usuarioOk2")
                .password(passwordEncoder.encode("usuarioOk2"))
                .build();
    }

    public static UsuarioForm createWithouNome() {
        UsuarioForm usuarioForm = createOK1();
        usuarioForm.setNome("");
        return usuarioForm;
    }

    public static UsuarioForm createWithouUsername() {
        UsuarioForm usuarioForm = createOK1();
        usuarioForm.setUsername("");
        return usuarioForm;
    }

    public static UsuarioForm createWithouPassword() {
        UsuarioForm usuarioForm = createOK1();
        usuarioForm.setPassword("");
        return usuarioForm;
    }

    public static UsuarioForm createInvalidEmail() {
        UsuarioForm usuarioForm = createOK1();
        usuarioForm.setEmail("teste.teste.teste.teste.com");
        return usuarioForm;
    }

    public static UsuarioDto createResponse1() {
        return UsuarioDto
                .builder()
                .email("teste@gmail.com")
                .nome("Teste Usuario 1")
                .username("usuarioOk1")
                .id(1l)
                .build();
    }

    public static UsuarioDto createResponse2() {
        return UsuarioDto
                .builder()
                .id(2l)
                .email("teste@gmail.com")
                .nome("Teste Usuario 2")
                .username("usuarioOk2")
                .build();
    }
}
