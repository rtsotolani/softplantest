//package br.com.rodolpho.sotolani.pessoas.persistence;
//
//import com.humanoo.booksapi.book.Book;
//import com.humanoo.booksapi.book.BookRepository;
//import com.humanoo.booksapi.book.BookService;
//import com.humanoo.booksapi.controller.book.request.BookRequest;
//import com.humanoo.booksapi.controller.book.response.AuthorResponse;
//import com.humanoo.booksapi.controller.book.response.BookResponse;
//import com.humanoo.booksapi.controller.book.response.CategoryResponse;
//import com.humanoo.booksapi.controller.book.response.MinimalBookResponse;
//import com.humanoo.booksapi.exception.BookNotFoundException;
//import com.humanoo.booksapi.mock.BookMockFactory;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.dao.DataIntegrityViolationException;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.*;
//
//import static org.hamcrest.Matchers.contains;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertThat;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class UsuarioPersistenceTests {
//    @Autowired
//    BookService bookService;
//    @Autowired
//    BookRepository bookRepository;
//
//    @Before
//    public void setUp() {
//        bookRepository.deleteAll();
//    }
//
//    @Test
//    public void insertBookOK_ShouldInsertTheBookCorrectly() {
//        BookRequest bookRequest = BookMockFactory.createBookOK1();
//
//        BookResponse saved = bookService.insertBook(bookRequest);
//
//        assertBookIsCorrectlySaved(saved);
//    }
//
//    @Test
//    public void updateBookOK_ShouldUpdateTheBookCorrectly() {
//        BookRequest bookOK1 = BookMockFactory.createBookOK1();
//        BookRequest bookOK2 = BookMockFactory.createBookOK2();
//        BookResponse insertedBook = bookService.insertBook(bookOK1);
//
//        BookResponse updatedBook = bookService.updateBook(bookOK2, insertedBook.getId());
//
//        assertBookIsCorrectlySaved(updatedBook);
//    }
//
//    @Test
//    public void getAllBooks_ShouldReturnBooksWithMinimalDetails() {
//        BookRequest bookOK1 = BookMockFactory.createBookOK1();
//        BookRequest bookOK2 = BookMockFactory.createBookOK2();
//
//        BookResponse bookResponse1 = bookService.insertBook(bookOK1);
//        BookResponse bookResponse2 = bookService.insertBook(bookOK2);
//
//        List<MinimalBookResponse> allBooks = bookService.getAllBooks();
//        assertBooksReturnedWithMinimalDetails(bookResponse1, bookResponse2, allBooks);
//    }
//
//    @Test
//    public void getBookDetails_ShouldReturnTheBookCorrectly() {
//        BookRequest bookOK1 = BookMockFactory.createBookOK1();
//        BookResponse bookResponse = bookService.insertBook(bookOK1);
//
//        BookResponse bookDetails = bookService.getBookDetails(bookResponse.getId());
//
//        assertEquals(bookResponse, bookDetails);
//    }
//
//
//    @Test(expected = DataIntegrityViolationException.class)
//    public void insertBookWithForeignKeyProblem_ShouldThrowException() {
//        BookRequest bookWithInexistentAuthor = BookMockFactory.createBookWithInexistentAuthor();
//        bookService.insertBook(bookWithInexistentAuthor);
//    }
//
//    @Test(expected = BookNotFoundException.class)
//    public void findByIdWithEmptyDatabase_ShouldThrowException() {
//        long inexistentId = 1L;
//        bookService.findById(inexistentId);
//    }
//
//    private void assertBookIsCorrectlySaved(BookResponse bookResponse) {
//        Book book = bookService.findById(bookResponse.getId());
//        assertFieldsMatch(bookResponse, book);
//    }
//
//    private void assertFieldsMatch(BookResponse bookResponse, Book book) {
//        assertEquals(bookResponse.getTitle(), book.getTitle());
//        assertEquals(bookResponse.getIsbn(), book.getIsbn());
//        assertEquals(bookResponse.getAuthor(), AuthorResponse.fromEntity(book.getAuthor()));
//        assertEquals(bookResponse.getCreatedAt(), new Date(book.getCreatedAt()));
//        assertEquals(bookResponse.getUpdatedAt(), new Date(book.getUpdatedAt()));
//        assertCategoryListMatches(bookResponse, book);
//    }
//
//    private void assertCategoryListMatches(BookResponse bookResponse, Book book) {
//        assertEquals(bookResponse.getCategoryList().size(), book.getCategoryList().size());
//        book.getCategoryList().forEach(
//            category -> {assertThat(bookResponse.getCategoryList(), contains(CategoryResponse.fromEntity(category)));}
//        );
//    }
//
//    private void assertBooksReturnedWithMinimalDetails(BookResponse bookResponse1, BookResponse bookResponse2, List<MinimalBookResponse> allBooks) {
//        Set<MinimalBookResponse> returnedSet = new HashSet<>(allBooks);
//        Set<MinimalBookResponse> expectedSet = new HashSet<>(
//            Arrays.asList(
//                new MinimalBookResponse(bookResponse1.getId(), bookResponse1.getTitle(), bookResponse1.getAuthor().getName()),
//                new MinimalBookResponse(bookResponse2.getId(), bookResponse2.getTitle(), bookResponse2.getAuthor().getName())
//            )
//        );
//        assertEquals(expectedSet, returnedSet);
//    }
//}
